import 'dart:html';

import 'package:Internal_lore/app_component.template.dart' as ng;
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:http/browser_client.dart';

import 'main.template.dart' as self;

const OpaqueToken<String> AuthToken = OpaqueToken<String>('Authorization');
const OpaqueToken<String> URLBaseToken = OpaqueToken<String>('URLBase');
const OpaqueToken<String> URLRedirectToken = OpaqueToken<String>('URLRedirect');

final String token = getToken();
final String url = getBaseURL();
@GenerateInjector([
  ClassProvider(BrowserClient),
  routerProvidersHash,
  FactoryProvider.forToken(AuthToken, getToken),
  FactoryProvider.forToken(URLBaseToken, getBaseURL),
  FactoryProvider.forToken(URLRedirectToken, getRedirectURL),
])
final InjectorFactory injector = self.injector$Injector;

void main() {
  runApp(ng.AppComponentNgFactory, createInjector: injector);
}

String getToken() {
  for (String cookie in document.cookie.split(";")) {
    List<String> KVPair = cookie.split("=");
    if (KVPair[0] == "auth-token") {
      return "Bearer " + KVPair[1];
    }
  }
//  window.location.replace('');
  return "Bearer undefined";
}

String getBaseURL() {
  String localUrl = window.localStorage['url_base'];
  return (localUrl == null) ? "https://polosero.pythonanywhere.com" : localUrl;
}

String getRedirectURL() {
  String localUrl = window.localStorage['url_redirect'];
  return (localUrl == null) ? "https://login.polo-sero.cz/" : localUrl;
}
