part of model;

enum InputType { select, text, number, datetime, boolean }

class ValueOption {
  String id;
  String label;

  ValueOption(this.id, this.label);
}

abstract class Rule<T> {
  RuleType rType;
  T value;

  Rule(this.value, this.rType);

  Map<String, dynamic> toMap();
}

class RuleType {
  InputType type;
  String name, label;
  Map<String, ValueOption> values;
  StringSelectionOptions<String> selectionOption;

  RuleType(Map<String, dynamic> data) {
    this.name = data["name"];
    this.label = data["label"];
    switch (data["type"]) {
      case "select":
        this.type = InputType.select;
        break;
      case "text":
        this.type = InputType.text;
        break;
      case "number":
        this.type = InputType.number;
        break;
      case "datetime":
        this.type = InputType.datetime;
        break;
      case "boolean":
        this.type = InputType.boolean;
        break;
      default:
        throw Error();
    }
    if (this.type == InputType.select) {
      values = Map<String, ValueOption>();
      data["values"].forEach((entry) {
        values[entry["value"].toString()] =
            ValueOption(entry["value"].toString(), entry["label"].toString());
      });
      selectionOption = StringSelectionOptions<String>(values.keys.toList(),
          toFilterableString: OptionRenderer);
    }
  }

  String OptionRenderer(dynamic s) => values[s].label;

  static String Renderer(dynamic s) => s.label;

  void UpdateRulesetType(Map<String, dynamic> data) {
    this.label = data["label"];
    switch (data["type"]) {
      case "select":
        this.type = InputType.select;
        break;
      case "text":
        this.type = InputType.text;
        break;
      case "number":
        this.type = InputType.number;
        break;
      case "datetime":
        this.type = InputType.datetime;
        break;
      case "boolean":
        this.type = InputType.boolean;
        break;
      default:
        throw Error();
    }
    if (this.type == InputType.select) {
      values.clear();
      data["values"].forEach((entry) {
        values[entry["value"].toString()] =
            ValueOption(entry["value"].toString(), entry["label"].toString());
      });
      selectionOption = StringSelectionOptions<String>(values.keys.toList(),
          toFilterableString: OptionRenderer);
    }
  }
}

class DateTimeRule extends Rule<DateTime> {
  DateTimeRule(String value, RuleType rType) : super(toDateTime(value), rType);

  @override
  Map<String, String> toMap() {
    String res = value.toString();
    if (res == "null") {
      return {rType.name: "null"};
    } else {
      return {rType.name: res.substring(0, 19)};
    }
  }

  String toString() {
    Map<String, String> map = this.toMap();

    if (map.values.first == "null") return null;
    return rType.label + ": " + map.values.first.substring(0, 16);
  }

  static DateTime toDateTime(String value) {
    if (value == null) return null;
    return DateTime.tryParse(value);
  }
}

class NumberRule extends Rule<int> {
  NumberRule(int value, RuleType rType) : super(value, rType);

  @override
  Map<String, String> toMap() {
    return {rType.name: value.toString()};
  }

  String toString() {
    Map<String, String> map = this.toMap();
    if (map.values.first == "null") return null;
    return rType.label + ": " + map.values.first;
  }
}

class SelectRule extends Rule<String> {
  SelectRule(String value, RuleType type) : super(value, type);

  @override
  Map<String, String> toMap() {
    return {rType.name: value.toString()};
  }

  @override
  String toString() {
    Map<String, String> map = this.toMap();
    if (map.values.first == "null") return null;
    return rType.label + ": " + rType.values[value].label;
  }
}

class TextRule extends Rule<String> {
  TextRule(String value, RuleType rType) : super(value, rType);

  @override
  Map<String, String> toMap() {
    return {rType.name: value.toString()};
  }

  String toString() {
    Map<String, String> map = this.toMap();
    if (map.values.first == "null") return null;
    return rType.label + ": " + map.values.first;
  }
}

class BooleanRule extends Rule<bool> {
  BooleanRule(RuleType rType) : super(true, rType);

  @override
  Map<String, String> toMap() {
    return {rType.name: value.toString()};
  }

  String toString() {
    return rType.label;
  }
}
