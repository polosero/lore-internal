part of model;

class Document {
  //<editor-fold desc="Properties">
  Model model;
  int id;
  String name;
  bool setName;
  bool hasSnippets;
  List<Snippet> snippets;
  List<Ruleset> rulesets;

  //</editor-fold>

  //<editor-fold desc="Constructors">
  Document(Document doc, this.model) {
    id = doc.id;
    name = doc.name;
    setName = doc.setName;
    hasSnippets = doc.hasSnippets;
    snippets = List<Snippet>();
    rulesets = List<Ruleset>();
    doc.snippets.forEach((snippet) {
      snippets.add(Snippet.FromSnippet(snippet, this));
    });
    doc.rulesets.forEach((ruleset) {
      rulesets.add(Ruleset.FromRuleset(ruleset, Model.structures['document']));
    });
  }

  Document.WithoutSnippets(Map<String, dynamic> map, this.model) {
    id = map['id'];
    name = map['name'];
    setName = true;
    snippets = List<Snippet>();
    hasSnippets = false;
    rulesets = List<Ruleset>();
    map['rulesets'].forEach((data) {
      rulesets.add(Ruleset.FromMap(data, Model.structures['document']));
    });
  }

  //</editor-fold>

  //<editor-fold desc="Updating">
  void DeleteSnippetById(int id) {
    snippets.removeWhere((Snippet snip) => snip.snippetId == id);
    NormalizeOrder();
  }

  int MaxSnippetId() {
    int res = 0;
    for (Snippet snip in snippets) {
      if (res < snip.snippetId) res = snip.snippetId;
    }
    return res;
  }

  void NormalizeOrder() {
    int i = 1;
    snippets.forEach((Snippet snip) => snip.order = i++);
  }

  void SortSnippets() {
    snippets.sort((Snippet a, Snippet b) => a.order - b.order);
  }

  void UpdateDocument(Map<String, dynamic> map) {
    name = map['name'];
    setName = true;
    rulesets.clear();
    map['rulesets'].forEach((data) {
      rulesets.add(Ruleset.FromMap(data, Model.structures['document']));
    });
    if (hasSnippets) UpdateSnippets();
  }

  Future<void> UpdateSnippets() async {
    if (hasSnippets) {
      await model.networkService.GetDocumentSnippetsData(id).then((list) {
        for (Snippet snip in snippets) {
          int index = list.indexWhere((data) => data['id'] == snip.snippetId);
          if (index != -1) {
            snip.UpdateSnippet(list.removeAt(index));
          } else {
            snippets.remove(snip);
          }
        }
        list.forEach((data) {
          snippets.add(Snippet.FromMap(data, this));
        });
        SortSnippets();
        NormalizeOrder();
      });
    } else {
      await _GetSnippets();
    }
  }

  Future<void> _GetSnippets() async {
    hasSnippets = true;
    model.networkService.GetDocumentSnippetsData(id).then((list) {
      list.forEach((data) {
        snippets.add(Snippet.FromMap(data, this));
      });
    });
    NormalizeOrder();
  }

  static bool IsDifferent(Document newD, Document oldD) {
    if (newD.name != oldD.name ||
        newD.snippets.length != oldD.snippets.length ||
        newD.rulesets.length != oldD.rulesets.length) {
      return true;
    }
    for (int i = 0; i < newD.snippets.length; i++) {
      if (Snippet.Difference(newD.snippets[i], oldD.snippets[i]) != null) {
        return true;
      }
    }

    for (int i = 0; i < newD.rulesets.length; i++) {
      if (Ruleset.Difference(newD.rulesets[i], oldD.rulesets[i]) != null) {
        return true;
      }
    }
    return false;
  }

  static List<Map<String, dynamic>> RuleDiff(Document oldD, Document newD) {
    List<Map<String, dynamic>> resData = List<Map<String, dynamic>>();
    if (newD.rulesets.length != oldD.rulesets.length) {
      for (Ruleset ruleset in newD.rulesets) {
        Map<String, dynamic> data = ruleset.toMap();
        if (data.isEmpty) continue;
        resData.add(data);
      }
    } else {
      List<Map<String, dynamic>> old = List<Map<String, dynamic>>();
      for (Ruleset ruleset in newD.rulesets) {
        Map<String, dynamic> data = ruleset.toMap();
        if (data.isEmpty) continue;
        resData.add(data);
      }
      for (Ruleset ruleset in oldD.rulesets) {
        Map<String, dynamic> data = ruleset.toMap();
        if (data.isEmpty) continue;
        old.add(data);
      }
      if (jsonEncode(resData) == jsonEncode(old)) {
        resData = null;
      }
    }
    return resData;
  }
//</editor-fold>
}
