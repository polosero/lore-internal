part of model;

class Ruleset {
  List<Rule<dynamic>> rules;
  List<RuleType> typeList;
  String parsed = "";

  //<editor-fold desc="Constructors">
  Ruleset.FromMap(Map<String, dynamic> map, this.typeList) {
    rules = List<Rule<dynamic>>();
    map.entries.forEach((data) {
      if (data.value != null) {
        RuleType RType = typeList.firstWhere((r) => data.key == r.name);
        Rule<dynamic> rule = toRule(RType, data.value.toString());
        rules.add(rule);
      }
    });
    updateParsed();
  }

  Ruleset.FromRuleset(Ruleset ruleset, this.typeList) {
    rules = List<Rule<dynamic>>();
    ruleset.rules.forEach((rule) {
      Rule<dynamic> res = toRule(rule.rType, rule.value.toString());
      rules.add(res);
    });
    parsed = ruleset.parsed;
  }

  Ruleset.Prefilled(int j, this.typeList) {
    rules = List<Rule<dynamic>>();
    if (j > typeList.length) j = typeList.length;
    for (int i = 0; i < j; i++) {
      RuleType RType = typeList[i];
      Rule<dynamic> rule = toRule(RType, null);
      rules.add(rule);
    }
    updateParsed();
  }

  //</editor-fold>

  //<editor-fold desc="Updating">
  Map<String, dynamic> toMap() {
    if (rules.isEmpty) return null;
    Map<String, dynamic> result = Map<String, dynamic>();
    rules.forEach((rule) {
      Map<String, dynamic> res = rule.toMap();
      if (res.values.first.toString() != "null") {
        result.addAll(res);
      }
    });
    return result;
  }

  void updateParsed() {
    String res = "";
    for (int i = 0; i < rules.length; i++) {
      if (rules[i].toString() == null) continue;
      String add = rules[i].toString();
      if (add != null && add != "") {
        res += add;
        if (i < rules.length - 1) {
          res += " | ";
        }
      }
    }
    if (res == "") {
      res = Model.localization.currentLanguage
          .Render("empty_ruleset_placeholder");
    }
    parsed = res;
  }

  static Map<String, dynamic> Difference(Ruleset ruleset, Ruleset ruleset2) {
    Map<String, dynamic> oldR = ruleset.toMap(),
        newR = ruleset2.toMap();
    return (jsonEncode(oldR) == jsonEncode(newR)) ? null : newR;
  }

  static Rule<dynamic> toRule(RuleType type, String value) {
    Rule<dynamic> rule;
    switch (type.type) {
      case InputType.select:
        rule = SelectRule(value, type);
        break;
      case InputType.text:
        rule = TextRule(value, type);
        break;
      case InputType.number:
        if (value == null) value = "";
        rule = NumberRule(int.tryParse(value), type);
        break;
      case InputType.datetime:
        rule = DateTimeRule(value, type);
        break;
      case InputType.boolean:
        rule = BooleanRule(type);
        break;
    }
    return rule;
  }
//</editor-fold>
}
