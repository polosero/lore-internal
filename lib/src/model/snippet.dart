part of model;

class Snippet {
  //<editor-fold desc="Properties">
  int snippetId, documentId, order;
  Document document;
  String html, output;
  List<Ruleset> rulesets;
  bool setHtml;

  //</editor-fold>

  //<editor-fold desc="Constructors">
  Snippet.Empty(this.document) {
    snippetId = document.MaxSnippetId() + 1;
    documentId = document.id;
    order = document.snippets.length + 1;
    html = "";
    output = "";
    rulesets = List<Ruleset>();
    setHtml = true;
  }

  Snippet.FromMap(Map<String, dynamic> map, this.document) {
    snippetId = map["id"];
    documentId = map["document_id"];
    html = md2Html(map['content'].toString());
    output = html;
    order = map["document_order"];
    rulesets = List<Ruleset>();
    if (map["rulesets"] != null) {
      map["rulesets"].forEach((map) {
        rulesets.add(Ruleset.FromMap(map, Model.structures['snippet']));
      });
    }
    setHtml = true;
  }

  Snippet.FromSnippet(Snippet snip, this.document) {
    snippetId = snip.snippetId;
    documentId = snip.snippetId;
    order = snip.order;
    html = snip.html;
    output = snip.output;
    rulesets = List<Ruleset>();
    snip.rulesets.forEach((Ruleset r) {
      rulesets.add(Ruleset.FromRuleset(r, Model.structures['snippet']));
    });
    setHtml = snip.setHtml;
  }

  //</editor-fold>

  //<editor-fold desc="Converters">
  static String md2Html(String data) {
    return markdownToHtml(
        data
            .replaceAll('&', '&amp;')
            .replaceAll('<', '&lt;')
            .replaceAll('>', '&gt;'),
        blockSyntaxes: [EmptyLineBlockSyntax()]);
  }

  //</editor-fold>

  //<editor-fold desc="Updating">
  Map<String, dynamic> toMap() {
    Map<String, dynamic> res = Map<String, dynamic>();
    res["order"] = order.toString();
    res["content"] = html2md.convert(
        output, styleOptions: {"headingStyle": "atx"}, ignore: ['hr']);
    List<Map<String, dynamic>> resData = List<Map<String, dynamic>>();
    for (Ruleset rule in rulesets) {
      Map<String, dynamic> map = rule.toMap();
      if (map == null || map.isEmpty) continue;
      resData.add(map);
    }
    res["rulesets"] = jsonEncode(resData);
    return res;
  }

  void UpdateSnippet(Map<String, dynamic> map) {
    html = md2Html(map["content"].toString());
    output = html;
    order = map["document_order"];
    rulesets.clear();
    if (map["rulesets"] != null) {
      map["rulesets"].forEach((map) {
        rulesets.add(Ruleset.FromMap(map, Model.structures['snippet']));
      });
    }
    setHtml = true;
  }

  static Map<String, dynamic> Difference(Snippet oldS, Snippet newS) {
    Map<String, dynamic> dataO = oldS.toMap(),
        dataN = newS.toMap(),
        res = Map<String, dynamic>();
    if (dataO["order"] != dataN["order"]) {
      print("--DIF: Original order: " + dataO["order"].toString() +
          " New order: " + dataN["order"].toString());
      res['order'] = dataN["order"];
    }
    if (dataO["content"] != dataN["content"]) {
      print("--DIF: Original content: " + dataO["content"].toString() +
          " New content: " + dataN["content"].toString());
      res['content'] = dataN["content"];
    }
    if (dataO["rulesets"] != dataN["rulesets"]) {
      print("--DIF: Original rulesets: " + dataO["rulesets"].toString() +
          " New rulesets: " + dataN["rulesets"].toString());
      res['rulesets'] = dataN["rulesets"];
    }
    return res.isEmpty ? null : res;
  }
//</editor-fold>
}

class EmptyLineBlockSyntax extends BlockSyntax {
  RegExp get pattern => RegExp(r'^(?:[ \t][ \t]+)$');

  const EmptyLineBlockSyntax();

  Node parse(BlockParser parser) {
    parser.encounteredBlankLine = true;
    parser.advance();

    return Element('p', [Element.empty('br')]);
  }
}
