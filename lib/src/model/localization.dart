part of model;

class Localization {
  Language currentLanguage;
  Map<String, Language> languages;
  Model model;
  StringSelectionOptions<String> selectionOption;

  Localization(List<dynamic> data, this.model) {
    languages = Map<String, Language>();
    for (dynamic map in data) {
      languages[map["id"]] = Language.WithoutDataFromMap(map, model);
    }
    selectionOption = StringSelectionOptions<String>(languages.keys.toList(),
        toFilterableString: OptionRenderer);
    if (data.isEmpty) {
      currentLanguage = Language.Empty();
    }
  }

  String OptionRenderer(dynamic s) => languages[s].label;
}

class Language {
  Map<String, String> data;
  String label, id, url;
  bool hasData;
  Model model;

//  Language.WithoutData(this.label, this.id, this.url):hasData=false,data=Map<String,String>();
  Language.WithoutDataFromMap(dynamic map, this.model) {
    hasData = false;
    data = Map<String, String>();
    label = utf8.decode((map["label"] as String).codeUnits);
    id = map["id"];
    url = map["url"];
  }

  Future<void> GetData() async {
    Map<String, dynamic> map =
        await model.networkService.GetLanguageData(this.url);
    for (MapEntry<String, dynamic> entry in map.entries) {
      data[entry.key] = utf8.decode((entry.value as String).codeUnits);
    }
    hasData = true;
  }

  String Render(String key) {
    String res = data[key];
    return (res == null) ? key : res;
  }

  Language.Empty(){
    hasData = true;
    data = Map<String, String>();
    label = "No Language";
    id = "none";
    url = "none";
  }
}
