library model;

import 'dart:convert';

import 'package:Internal_lore/src/network/network_service.dart';
import 'package:angular_components/model/selection/string_selection_options.dart';
import 'package:html2md/html2md.dart' as html2md;
import 'package:markdown/markdown.dart';

part 'document.dart';

part 'localization.dart';

part 'rule.dart';

part 'ruleset.dart';

part 'snippet.dart';

class Model {
  //<editor-fold desc="Properties">
  static Map<String, List<RuleType>> structures;
  NetworkService networkService;
  List<Document> documentList;
  static Localization localization;

  bool updatingDocuments = false,
      updatingStructures = false;

  //</editor-fold>

  Model(this.networkService) {
    documentList = List<Document>();
    structures = Map<String, List<RuleType>>();
  }

  //<editor-fold desc="Document Handling">
  Future<Document> getDocument(int id) async {
    Document res;
    if (documentList.isEmpty) {
      await UpdateDocuments();
    }
    res = documentList
        .where((d) => d.id == id)
        .toList()
        .first;
    if (res == null) {
      await UpdateDocuments();
      res = documentList
          .where((d) => d.id == id)
          .toList()
          .first;
    }

    if (res != null) {
      res.UpdateSnippets();
    }
    return res;
  }

  //<editor-fold desc="Lock Handling">
  Future<Map<String, dynamic>> LockDocument(int id) async {
    Map<String, dynamic> data = await networkService.LockDocument(id);
    if (data["success"]) {
      await UpdateDocuments();
    }
    return data;
  }

  Future<void> UnlockDocument(int id) async {
    await networkService.UnlockDocument(id);
  }

  //</editor-fold>

  Future<bool> NewDocument(String name) async {
    if (await networkService.PostDocument(name)) {
      UpdateDocuments();
      return true;
    } else {
      return false;
    }
  }

  Future<bool> RemoveDocument(int id) async {
    if (await networkService.RemoveDocument(id)) {
      UpdateDocuments();
      return true;
    } else {
      return false;
    }
  }

  Future<bool> SaveDocument(int oId, Document document) async {
    Document originalDocument = await getDocument(oId),
        newDocument = Document(document, this);
    bool renamed = true;
    Map<String, dynamic> data = Map<String, dynamic>();

    if (originalDocument.name != newDocument.name)
      data["name"] = newDocument.name;
    List<Map<String, dynamic>> ruleMap =
    Document.RuleDiff(originalDocument, newDocument);
    if (ruleMap != null) data["rulesets"] = jsonEncode(ruleMap);
    if (data.isNotEmpty) {
      renamed = await networkService.UpdateDocument(data, oId);
    }
    print(
        "Updating document. ID: " + originalDocument.id.toString() + " Name: " +
            originalDocument.name);
    for (Snippet snip1 in originalDocument.snippets) {
      int index = newDocument.snippets
          .indexWhere((Snippet snip2) => snip1.snippetId == snip2.snippetId);
      print("ORIGINALSnippetID:" + snip1.snippetId.toString() +
          ((index == -1) ? " NOT FOUND" : "FOUND"));
      if (index != -1) {
        Map<String, dynamic> data =
        Snippet.Difference(snip1, newDocument.snippets[index]);
        if (data != null) {
          print("Differences found");
          await networkService.UpdateSnippet(oId, snip1.snippetId, data);
        }
        newDocument.snippets.removeAt(index);
      } else {
        await networkService.SnippetDelete(oId, snip1.snippetId);
      }
    }

    for (Snippet snip in newDocument.snippets) {
      await networkService.MakeSnippet(oId, snip.toMap());
    }
    await UpdateDocuments();
    print("Ended updating document. ID: " + originalDocument.id.toString() +
        " Name: " + originalDocument.name);
    return renamed;
  }

  //<editor-fold desc="Updating Documents">
  Future<void> UpdateDocuments() async {
    if (updatingDocuments) return;
    updatingDocuments = true;
    if (documentList.isEmpty) {
      await _GetDocuments();
    } else {
      await networkService.GetAllDocumentsData().then((list) {
        for (Document doc in documentList) {
          int index = list.indexWhere((data) => data['id'] == doc.id);
          if (index != -1) {
            doc.UpdateDocument(list.removeAt(index));
          } else {
            documentList.remove(doc);
          }
        }
        list.forEach((data) {
          documentList.add(Document.WithoutSnippets(data, this));
        });
      });
    }
    updatingDocuments = false;
    return;
  }

  Future<void> _GetDocuments() async {
    await networkService.GetAllDocumentsData().then((list) {
      list.forEach((data) {
        documentList.add(Document.WithoutSnippets(data, this));
      });
    });
    return;
  }

  //</editor-fold>
  //</editor-fold>

  //<editor-fold desc="Structures Handling">
  Future<void> UpdateStructures() async {
    if (updatingStructures) return;
    updatingStructures = true;
    if (structures.isEmpty) {
      await _GetStructures();
    } else {
      await networkService.GetStructures().then((map) {
        map.keys.forEach((String key) {
          for (int i = 0; i < structures[key].length;) {
            int index = map[key]
                .indexWhere((data) => data['name'] == structures[key][i].name);
            if (index != -1) {
              structures[key][i].UpdateRulesetType(map[key].removeAt(index));
              i++;
            } else {
              structures[key].removeAt(i);
            }
          }
          map[key].forEach((data) {
            structures[key].add(RuleType(data));
          });
        });
      });
    }
    updatingStructures = false;
    return;
  }

  Future<void> _GetStructures() async {
    await networkService.GetStructures().then((map) {
      map.keys.forEach((String key) {
        structures[key] = List<RuleType>();
        map[key].forEach((data) {
          structures[key].add(RuleType(data));
        });
      });
    });
    return;
  }

//</editor-fold>

  Future<void> SetLocalization({String id}) async {
    //todo:replace with default language from configuration
    if (id == null && localization == null) id = "cz";
    if (id == null) return;
    if (localization == null) {
      List<dynamic> data = await networkService.GetLocalizationLanguages();
      localization = Localization(data, this);
    }
    Language language = localization.languages[id];
    if (language == null || language.id == localization.currentLanguage?.id)
      return;
    if (!language.hasData) await language.GetData();
    localization.currentLanguage = language;
    documentList.forEach(((Document d) {
      d.rulesets.forEach((Ruleset r) {
        r.updateParsed();
      });
      d.snippets.forEach((Snippet s) {
        s.rulesets.forEach((Ruleset r) {
          r.updateParsed();
        });
      });
    }));
  }
}
