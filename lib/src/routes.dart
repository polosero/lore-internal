import 'package:Internal_lore/src/routes_path.dart';
import 'package:Internal_lore/src/view/menu/view_menu_component.template.dart'
as menu_template;
import 'package:Internal_lore/src/view/view_document/view_document_component.template.dart'
    as document_template;
import 'package:angular_router/angular_router.dart';

export 'package:Internal_lore/src/routes_path.dart';

//import 'package:Internal_lore/src/view/view_document_list/view_document_list_component.template.dart'
//    as document_list_template;

class Routes {
  static final MENU = RouteDefinition(
    routePath: RoutePaths.MENU,
    component: menu_template.ViewMenuComponentNgFactory,
  );

  static final DOCUMENT_VIEW = RouteDefinition(
    routePath: RoutePaths.DOCUMENT_VIEW,
    component: document_template.ViewDocumentComponentNgFactory,
  );

  static final all = <RouteDefinition>[
    MENU,
    DOCUMENT_VIEW,
    RouteDefinition.redirect(path: '', redirectTo: RoutePaths.MENU.toUrl()),
  ];
}
