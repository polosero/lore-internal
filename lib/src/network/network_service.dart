import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'dart:core' as prefix0;

import 'package:Internal_lore/src/network/gateway_service.dart';
import 'package:http/http.dart';

class NetworkService {
  final Gateway _gateway;

  NetworkService(this._gateway);

  //<editor-fold desc="Structures Handling">
  Future<Map<String, dynamic>> GetStructures() async {
    try {
      Response res = await _gateway.get({"endPoint": "/documents/struct"});
      if (res.statusCode == 200) return jsonDecode(res.body);
    } catch (e) {}
    return Map<String, List<dynamic>>();
  }

  //</editor-fold>

  //<editor-fold desc="Lock Handling">
  Future<Map<String, dynamic>> LockDocument(int id) async {
    Response response;
    Map<String, dynamic> res = Map<String, dynamic>();
    try {
      response = await _gateway.put({"endPoint": "/documents/$id/lock"});
      res["success"] = response.statusCode == 200;
      if (res["success"]) {
        Map<String, dynamic> data = jsonDecode(response.body);
        res["fresh"] = data["fresh"] == 1;
        RegExp reg = RegExp(r"(\d+)");
        Iterable<Match> matches = reg.allMatches(data["length"]);
        String longestNumber = "";
        for (Match m in matches) {
          String match = m.group(0);
          if (match.length > longestNumber.length) longestNumber = match;
        }
        res["time"] = int.parse(longestNumber);
      }
    } catch (e) {}
    return res;
  }

  Future<void> UnlockDocument(int id) async {
    try {
      await _gateway.delete({"endPoint": "/documents/$id/lock"});
    } catch (e) {}
  }

  //</editor-fold>

  //<editor-fold desc="Document Handling">
  Future<bool> PostDocument(String name) async {
    Response res;
    try {
      res = await _gateway.post({
        "endPoint": "/documents/",
        "body": {"name": name}
      });
    } catch (e) {}
    return res.statusCode == 200;
  }

  Future<bool> RemoveDocument(int id) async {
    Response res;
    try {
      res = await _gateway.delete({"endPoint": "/documents/$id"});
    } catch (e) {}
    return res.statusCode == 200;
  }

  Future<List<dynamic>> GetAllDocumentsData() async {
    Response res = await _gateway.get({"endPoint": "/documents/"});
    if (res.statusCode == 200)
      return jsonDecode(res.body);
    else
      return List<dynamic>();
  }

  Future<bool> UpdateDocument(dynamic data, int id) async {
    Response res;
    try {
      res = await _gateway.put({"endPoint": "/documents/$id", "body": data});
    } catch (e) {}
    return res.statusCode == 200;
  }

  //</editor-fold>

  //<editor-fold desc="Snippet Handling">
  Future<void> UpdateSnippet(int documentId, int snippetId,
      dynamic data) async {
    try {
      await _gateway
          .put({"endPoint": "/documents/$documentId/$snippetId", "body": data});
    } catch (e) {}
  }

  Future<void> SnippetDelete(int documentId, int snippetId) async {
    try {
      await _gateway.delete({"endPoint": "/documents/$documentId/$snippetId"});
    } catch (e) {}
  }

  Future<List<dynamic>> GetDocumentSnippetsData(int id) async {
    Response res = await _gateway.get({"endPoint": "/documents/$id"});
    return jsonDecode(res.body);
  }

  Future<void> MakeSnippet(int id, Map<String, dynamic> map) async {
    try {
      await _gateway.post({"endPoint": "/documents/$id", "body": map});
    } catch (e) {}
  }

//</editor-fold>

  Future<List<dynamic>> GetLocalizationLanguages() async {
    Response res = await _gateway.getURL("./localization.json");
    if (res.statusCode == 200) {
      return jsonDecode(res.body);
    } else {
      return List<dynamic>();
    }
  }

  Future<Map<String, dynamic>> GetLanguageData(String url) async {
    Response res = await _gateway.getURL("./${url}");
    return jsonDecode(res.body);
  }
}
