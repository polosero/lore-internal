import 'package:angular_router/angular_router.dart';

class RoutePaths {
  static final MENU = RoutePath(path: 'menu');
  static final DOCUMENT_VIEW = RoutePath(path: 'document_view/:id');
}
