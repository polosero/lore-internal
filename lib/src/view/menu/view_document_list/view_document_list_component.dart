import 'dart:html' as HTML;
import 'dart:async';
import 'package:tuple/tuple.dart';

//<editor-fold desc="Internal Lore Imports">
import 'package:Internal_lore/src/model/model.dart';
import 'package:Internal_lore/src/routes_path.dart';
import 'package:Internal_lore/src/view/dialogs/new_document/new_document_component.dart';
import 'package:Internal_lore/src/view/dialogs/configure_language/configure_language_component.dart';
import 'package:Internal_lore/src/view/directives/clickable_directive.dart';
import 'package:Internal_lore/src/view/directives/localization_pipe.dart';
import 'package:Internal_lore/src/view/utils/rulesets/rulesets_component.dart';
//</editor-fold>

//<editor-fold desc="Angular Imports">
import 'package:angular/angular.dart';
import 'package:angular_components/laminate/overlay/module.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_icon/material_icon.dart';
import 'package:angular_router/angular_router.dart';
//</editor-fold>

@Component(
    selector: 'view-document-list',
    templateUrl: 'view_document_list_component.html',
    styleUrls: [
      "view_document_list_component.css"
    ],
    directives: [
      MaterialButtonComponent,
      coreDirectives,
      MaterialIconComponent,
      ClickableDirective,
      RulesetsComponent,
      NewDocumentDialog,
      ConfigureLanguageDialog
    ],
    providers: [
      overlayBindings,
    ],
    pipes: [
      LocalizationPipe
    ],
    exports: [
      Model
    ])
class ViewDocumentListComponent implements OnActivate, OnDestroy {
  bool showNewDocumentDialog = false,
      showConfigureLanguageDialog = false;
  Model model;
  Router _router;
  HTML.Element body;
  Map<Tuple4<String, bool, bool, bool>, Tuple2<KeyHandle, KeyHandle>> _mapping =
  Map<Tuple4<String, bool, bool, bool>, Tuple2<KeyHandle, KeyHandle>>();
  StreamSubscription bodyStreamOnKeyDown;
  StreamSubscription bodyStreamOnKeyUp;

  ViewDocumentListComponent(this.model, this._router) {
    body = HTML.document.body;
    setMapping();
  }

  //<editor-fold desc="Overrides">
  @override
  void onActivate(RouterState, RouterState current) {
    Future.wait([model.UpdateStructures(), model.SetLocalization()]).then((_) {
          () async {
        model.UpdateDocuments();
      }();
    });
    bodyStreamOnKeyDown = body.onKeyDown.listen(handleKeyboardInputDown);
    bodyStreamOnKeyUp = body.onKeyUp.listen(handleKeyboardInputUp);
  }

  @override
  void ngOnDestroy() {
    bodyStreamOnKeyDown.cancel();
    bodyStreamOnKeyUp.cancel();
  }

  //</editor-fold>

  void reload() {
    onActivate(null, null);
  }

  Object trackByDocumentId(_, dynamic o) => o is Document ? o.id : o;

  void viewDocument(int documentId) =>
      _router.navigate(
          RoutePaths.DOCUMENT_VIEW.toUrl(parameters: {"id": "$documentId"}));

  bool dialogOpen() {
    return showNewDocumentDialog ||
        showConfigureLanguageDialog;
  }

  void dialogClose() {
    showNewDocumentDialog = false;
    showConfigureLanguageDialog = false;
  }

  //<editor-fold desc="KeybindResolver">
  bool resolveKey(String code, bool ctrl, alt, shift, down) {
    Tuple2<KeyHandle, KeyHandle> tuple =
    _mapping[Tuple4<String, bool, bool, bool>(code, ctrl, alt, shift)];
    KeyHandle op = down ? tuple?.item1 : tuple?.item2;
    var ret = false;
    if (op != null) ret = op();
    return ret;
  }

  void handleKeyboardInputDown(HTML.KeyboardEvent event) {
    if (resolveKey(
        event.code, event.ctrlKey, event.altKey, event.shiftKey, true)) {
      event.preventDefault();
      event.stopPropagation();
    }
  }

  void handleKeyboardInputUp(HTML.KeyboardEvent event) {
    if (resolveKey(
        event.code, event.ctrlKey, event.altKey, event.shiftKey, false)) {
      event.preventDefault();
      event.stopPropagation();
    }
  }

  void setMapping() {
    //<editor-fold desc="Escape Handling">
    _mapping[Tuple4<String, bool, bool, bool>("Escape", false, false, false)] =
        Tuple2<KeyHandle, KeyHandle>(() => dialogOpen(), () {
          bool ret = dialogOpen();
          if (dialogOpen()) dialogClose();
          return ret;
        });
    //</editor-fold>
  }
//</editor-fold>
}

typedef KeyHandle();