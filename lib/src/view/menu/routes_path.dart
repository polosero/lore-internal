import 'package:Internal_lore/src/routes_path.dart' as _parent;
import 'package:angular_router/angular_router.dart';

class RoutePaths {
  static final MENU = RoutePath(path: '', parent: _parent.RoutePaths.MENU);
  static final DOCUMENT_LIST =
      RoutePath(path: 'document_list', parent: _parent.RoutePaths.MENU);
}
