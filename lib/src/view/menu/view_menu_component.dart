import 'package:Internal_lore/src/view/directives/time_pipe.dart';
import 'package:Internal_lore/src/view/menu/common_service.dart';
import 'package:Internal_lore/src/view/menu/routes.dart';
import 'package:angular/angular.dart';
import 'package:angular_components/laminate/popup/module.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_icon/material_icon.dart';
import 'package:angular_components/material_menu/material_menu.dart';
import 'package:angular_components/model/menu/menu.dart';
import 'package:angular_components/model/ui/icon.dart';
import 'package:angular_router/angular_router.dart';
//import 'dart:async';

enum MenuChosen { documents }

@Component(
    selector: 'view-menu',
    templateUrl: 'view_menu_component.html',
    styleUrls: [
      "view_menu_component.css"
    ],
    directives: [
      MaterialButtonComponent,
      MaterialIconComponent,
      MaterialMenuComponent,
      routerDirectives,
      coreDirectives
    ],
    providers: [
      popupBindings,
      ClassProvider(CommonService)
    ],
    exports: [
      RoutePaths,
      Routes,
      MenuChosen,
//      Model
    ],
    pipes: [
      TimePipe
    ])
class ViewMenuComponent implements OnActivate {
  //todo: rework this
  String menuClasses = "menu-popup";
  List<MenuItem> options = List<MenuItem<String>>();
  MenuModel<MenuItem> menuModel;
  Router _router;
  CommonService commonService;
  MenuChosen chosen;

  ViewMenuComponent(this.commonService, this._router) {
    options.add(MenuItem<String>("Document", action: SelectDocuments));
    menuModel = MenuModel<MenuItem>([MenuItemGroup<MenuItem>(options)],
        icon: Icon("menu"));
  }

  @override
  void onActivate(RouterState previous, RouterState current) {
    String currentUrl = current.routePath.toUrl();
    if (currentUrl == RoutePaths.DOCUMENT_LIST.toUrl()) {
      chosen = MenuChosen.documents;
    } else {
      throw Error;
    }
  }

  void SelectDocuments() {
    _router.navigate(RoutePaths.DOCUMENT_LIST.toUrl());
    chosen = MenuChosen.documents;
  }
}
