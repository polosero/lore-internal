import 'package:Internal_lore/src/view/menu/routes_path.dart';
import 'package:Internal_lore/src/view/menu/view_document_list/view_document_list_component.template.dart'
as document_list_template;
import 'package:angular_router/angular_router.dart';

export 'package:Internal_lore/src/view/menu/routes_path.dart';

class Routes {
  static final DOCUMENT_LIST = RouteDefinition(
    routePath: RoutePaths.DOCUMENT_LIST,
    component: document_list_template.ViewDocumentListComponentNgFactory,
  );

  static final MENU = RouteDefinition.redirect(
      routePath: RoutePaths.MENU, redirectTo: RoutePaths.DOCUMENT_LIST.toUrl());
  static final all = <RouteDefinition>[DOCUMENT_LIST, MENU];
}
