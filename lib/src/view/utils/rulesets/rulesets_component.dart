import 'dart:async';
import 'dart:core';

import 'package:Internal_lore/src/model/model.dart';
import 'package:Internal_lore/src/view/directives/clickable_directive.dart';
import 'package:angular/angular.dart';
import 'package:angular_components/material_chips/material_chip.dart';
import 'package:angular_components/material_chips/material_chips.dart';

@Component(
    selector: 'rulesets',
    templateUrl: 'rulesets_component.html',
    directives: [
      coreDirectives,
      ClickableDirective,
      MaterialChipComponent,
      MaterialChipsComponent,
    ],
    styleUrls: [
      "rulesets_component.css"
    ])
class RulesetsComponent implements OnDestroy {
  final _removeRuleset = StreamController<Ruleset>();
  final _editRuleset = StreamController<Ruleset>();

  @Input()
  List<Ruleset> rulesets;
  @Input()
  bool show;
  @Input()
  bool clickable;
  @Input()
  bool removable;

  RulesetsComponent();

  @Output()
  Stream<Ruleset> get editRulesetStream => _editRuleset.stream;

  @Output()
  Stream<Ruleset> get removeRulesetStream => _removeRuleset.stream;

  void editRuleset(Ruleset r) {
    _editRuleset.add(r);
  }

  @override
  void ngOnDestroy() {
    _removeRuleset.close();
    _editRuleset.close();
  }

  void removeRuleset(Ruleset r) {
    _removeRuleset.add(r);
  }

  Object trackByIndex(int index, _) {
    return index;
  }
}
