import 'dart:async';
import 'dart:core';
import 'dart:html' as HTML;

//<editor-fold desc="Internal Lore Imports">
import 'package:Internal_lore/src/model/model.dart';
import 'package:Internal_lore/src/view/directives/clickable_directive.dart';
import 'package:Internal_lore/src/view/directives/localization_pipe.dart';
import 'package:Internal_lore/src/view/utils/rulesets/rulesets_component.dart';

//</editor-fold>
import 'package:angular/angular.dart';

//<editor-fold desc="Angular Components Imports">
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_icon/material_icon.dart';
//</editor-fold>

@Component(
    selector: 'snippet-comp',
    templateUrl: 'view_snippet_component.html',
    directives: [
      coreDirectives,
      ClickableDirective,
      MaterialIconComponent,
      MaterialButtonComponent,
      RulesetsComponent,
    ],
    styleUrls: [
      "view_snippet_component.css"
    ],
    pipes: [
      LocalizationPipe
    ],
    exports: [
      Model
    ])
class ViewSnippetComponent
    implements OnDestroy, AfterViewChecked, AfterViewInit {
  //<editor-fold desc="Streams">
  final _moveMe = StreamController<Snippet>();
  final _moveTo = StreamController<Map<String, dynamic>>();
  final _removeMe = StreamController<int>();
  final _startEdit = StreamController<bool>();
  final _editRuleset = StreamController<Ruleset>();
  StreamSubscription focus;
  StreamSubscription blur;
  StreamSubscription dblClick;

  //</editor-fold>

  //<editor-fold desc="Outputs">
  @Output()
  Stream<Ruleset> get editRulesetStream => _editRuleset.stream;

  @Output()
  Stream<int> get removeMeStream => _removeMe.stream;

  @Output()
  Stream<Snippet> get moveMeStream => _moveMe.stream;

  @Output()
  Stream<bool> get startEditStream => _startEdit.stream;

  @Output()
  Stream<Map<String, dynamic>> get moveToStream => _moveTo.stream;

  //</editor-fold>

  //<editor-fold desc="Inputs">
  @Input()
  Snippet snippet;
  @Input()
  bool showMetadata;
  @Input()
  bool moveMode;
  @Input()
  bool moving;
  @Input()
  bool allowEdit = false;

  //</editor-fold>

  //<editor-fold desc="View Child">
  @ViewChild("contentElement")
  HTML.DivElement contentElement;
  @ViewChild("snippetElement")
  HTML.DivElement snippetElement;

  //</editor-fold>

  //<editor-fold desc="Rulesets">
  void addNewRuleset() {
    snippet.rulesets.add(Ruleset.Prefilled(4, Model.structures['snippet']));
  }

  void editRuleset(Ruleset r) {
    _editRuleset.add(r);
  }

  void removeRuleset(Ruleset r) {
    snippet.rulesets.remove(r);
  }

  //</editor-fold>

  //<editor-fold desc="Moving">
  void move() {
    _moveMe.add(snippet);
  }

  void stopMoving() {
    _moveMe.add(null);
  }

  void moveTo(bool above) {
    _moveTo.add({"above": above, "snippet": snippet});
  }

  //</editor-fold>

  void destroyMe() {
    _removeMe.add(snippet.snippetId);
  }

  //<editor-fold desc="Template service">
  void update(HTML.Event event) {
    snippet.output = (event.target as HTML.Element).innerHtml;
  }

  void setHtml() {
    if (contentElement != null && snippet != null) {
      contentElement.innerHtml = snippet.html;
    }
  }

  void onFocusHandler(_) {
    snippetElement.classes.add("focus");
  }

  void onBlurHandler(_) {
    snippetElement.classes.remove("focus");
  }

  //</editor-fold>

  void onDoubleClickHandler(HTML.Event e) {
    e.preventDefault();
    e.stopPropagation();
    if (!allowEdit && (e as HTML.MouseEvent).altKey) _startEdit.add(true);
  }

  //<editor-fold desc="Trackers">
  Object trackByIndex(int index, _) {
    return index;
  }

  //</editor-fold>

  //<editor-fold desc="Overrides">
  @override
  void ngOnDestroy() {
    _removeMe.close();
    _editRuleset.close();
    _moveMe.close();
    _moveTo.close();
    _startEdit.close();
    blur.cancel();
    focus.cancel();
    dblClick.cancel();
  }

  @override
  void ngAfterViewChecked() {
    if (snippet.setHtml) {
      snippet.setHtml = false;
      setHtml();
    }
  }

  @override
  void ngAfterViewInit() {
    focus = contentElement.onFocus.listen(onFocusHandler);
    blur = contentElement.onBlur.listen(onBlurHandler);
    dblClick = contentElement.onDoubleClick.listen(onDoubleClickHandler);
  }

//</editor-fold>
}
