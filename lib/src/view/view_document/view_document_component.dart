//<editor-fold desc="Miscellaneous Imports">
import 'dart:async';
import 'dart:html' as HTML;
import 'package:tuple/tuple.dart';
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'dart:js';

//</editor-fold>
//<editor-fold desc="Internal Lore Imports">
import 'package:Internal_lore/src/model/model.dart';
import 'package:Internal_lore/src/routes.dart';

//<editor-fold desc="Dialog Imports">
import 'package:Internal_lore/src/view/dialogs/cannot_edit_1/cannot_edit_1_component.dart';
import 'package:Internal_lore/src/view/dialogs/cannot_edit_2/cannot_edit_2_component.dart';
import 'package:Internal_lore/src/view/dialogs/delete_document/delete_document_component.dart';
import 'package:Internal_lore/src/view/dialogs/edit_ruleset/edit_ruleset_component.dart';
import 'package:Internal_lore/src/view/dialogs/remove_snippet/remove_snippet_component.dart';
import 'package:Internal_lore/src/view/dialogs/rename_unsuccessful/rename_unsuccessful_component.dart';
import 'package:Internal_lore/src/view/dialogs/save_changes/save_changes_component.dart';
import 'package:Internal_lore/src/view/dialogs/configure_language/configure_language_component.dart';
import 'package:Internal_lore/src/view/dialogs/delete_document_error/delete_document_error_component.dart';

//</editor-fold>
import 'package:Internal_lore/src/view/directives/time_pipe.dart';
import 'package:Internal_lore/src/view/directives/clickable_directive.dart';
import 'package:Internal_lore/src/view/directives/localization_pipe.dart';
import 'package:Internal_lore/src/view/utils/rulesets/rulesets_component.dart';
import 'package:Internal_lore/src/view/view_document/view_snippet/view_snippet_component.dart';

//</editor-fold>
//<editor-fold desc="Angular Components Imports">
import 'package:angular_components/laminate/overlay/module.dart';
import 'package:angular_components/laminate/popup/module.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_icon/material_icon.dart';
//</editor-fold>

@Component(
    selector: 'view-document',
    templateUrl: 'view_document_component.html',
    directives: [
      MaterialButtonComponent,
      coreDirectives,
      MaterialIconComponent,
      ViewSnippetComponent,
      RulesetsComponent,
      ClickableDirective,
      EditRulesetDialog,
      CannotEdit1Dialog,
      CannotEdit2Dialog,
      DeleteDocumentDialog,
      DeleteDocumentErrorDialog,
      RemoveSnippetDialog,
      RenameUnsuccessfulDialog,
      SaveChangesDialog,
      ConfigureLanguageDialog
    ],
    pipes: [
      TimePipe,
      LocalizationPipe
    ],
    providers: [
      overlayBindings,
      popupBindings
    ],
    styleUrls: [
      "view_document_component.css"
    ],
    exports: [
      Model
    ])
class ViewDocumentComponent implements OnActivate, OnDestroy, AfterViewChecked {
  //<editor-fold desc="Properties">
  Model model;
  Router _router;
  Document document;
  Snippet movingSnippet;
  Ruleset editableRuleset;
  int idToRemove;
  HTML.Element waitElement, body;
  Timer timer;
  Duration lockDuration;
  Map<Tuple4<String, bool, bool, bool>, Tuple2<KeyHandle, KeyHandle>> _mapping =
  Map<Tuple4<String, bool, bool, bool>, Tuple2<KeyHandle, KeyHandle>>();
  StreamSubscription bodyStreamOnKeyDown;
  StreamSubscription bodyStreamOnKeyUp;

  @ViewChild("title")
  HTML.HeadingElement title;

  //</editor-fold>

  //<editor-fold desc="State">
  bool allowEdit = false,
      showMetadata = true,
      moveMode = false;

  //</editor-fold>

  //<editor-fold desc="Dialog Booleans">
  bool showSaveChangesDialog = false,
      showRemoveSnippetDialog = false,
      showCannotEditDialog1 = false,
      showCannotEditDialog2 = false,
      showDeleteDocumentDialog = false,
      showDeleteDocumentErrorDialog = false,
      showRenameUnsuccessfulDialog = false,
      showEditRulesetDialog = false,
      showConfigureLanguageDialog = false;

  //</editor-fold>

  ViewDocumentComponent(this.model, this._router) {
    waitElement = HTML.document.body.querySelector('div#wait-overlay');
    body = HTML.document.body;
    setMapping();
  }

  void addNewRuleset() {
    document.rulesets.add(Ruleset.Prefilled(4, Model.structures['document']));
  }

  void addNewSnippet() {
    document.snippets.add(Snippet.Empty(document));
    HTML.document.execCommand("defaultParagraphSeparator", false, "p");
  }

  void editRuleset(Ruleset r) {
    editableRuleset = r;
    showEditRulesetDialog = true;
  }

  //<editor-fold desc="Lock Handling">
  void extendLock() async {
    Map<String, dynamic> data = await model.LockDocument(document.id);
    if (data["success"]) {
      if (data["fresh"]) {
        showCannotEditDialog2 = true;
      } else {
        timer.cancel();
        lock(data);
      }
    } else {
      showCannotEditDialog1 = true;
    }
  }

  void lock(Map<String, dynamic> data) {
    document = Document(document, model);
    allowEdit = true;
    lockDuration = Duration(minutes: data["time"]);
    timer = Timer.periodic(Duration(seconds: 1), (Timer t) {
      lockDuration -= Duration(seconds: 1);
      if (lockDuration.isNegative) t.cancel();
    });
  }

  //</editor-fold>

  void removeDocument() async {
    if (await model.RemoveDocument(document.id)) {
      _router.navigate(RoutePaths.MENU.toUrl());
    } else {
      showDeleteDocumentErrorDialog = true;
    }
  }

  void removeSnippet() {
    document.DeleteSnippetById(idToRemove);
  }

  void removeSnippetDialog(int id) {
    idToRemove = id;
    showRemoveSnippetDialog = true;
  }

  void saveChanges() async {
    waitElement.classes.add('active');
    showRenameUnsuccessfulDialog =
    !(await model.SaveDocument(document.id, document));
    waitElement.classes.remove('active');
  }

  //<editor-fold desc="Moving">
  void startMoveMode(Snippet snip) {
    movingSnippet = snip;
    moveMode = snip != null;
  }

  void move(Map<String, dynamic> data) {
    document.snippets.remove(movingSnippet);
    int index =
        document.snippets.indexOf(data['snippet']) + (data['above'] ? 0 : 1);
    document.snippets.insert(index, movingSnippet);
    document.NormalizeOrder();
    startMoveMode(null);
  }

  //</editor-fold>

  //<editor-fold desc="Edit">
  void saveChangesAndStopEdit() async {
    await saveChanges();
    if (!showRenameUnsuccessfulDialog) {
      stopEditProcedure(await model.getDocument(document.id));
      document = await model.getDocument(document.id);
    }
  }

  void startEdit() async {
    Map<String, dynamic> data = await model.LockDocument(document.id);
    if (data["success"]) {
      lock(data);
    } else {
      showCannotEditDialog1 = true;
    }
  }

  void stopEdit() async {
    Document oldDocument = await model.getDocument(document.id);
    if (Document.IsDifferent(document, oldDocument)) {
      showSaveChangesDialog = true;
    } else {
      stopEditProcedure(oldDocument);
    }
  }

  void stopEditProcedure(Document oldDocument) {
    timer.cancel();
    model.UnlockDocument(document.id);
    startMoveMode(null);
    allowEdit = false;
    idToRemove = null;
    editableRuleset = null;
    document = oldDocument;
  }

  void back() {
    if (allowEdit) {
      stopEditAndBackIfPossible();
    } else {
      _router.navigate(RoutePaths.MENU.toUrl());
    }
  }

  void stopEditAndBackIfPossible() async {
    Document oldDocument = await model.getDocument(document.id);
    if (Document.IsDifferent(document, oldDocument)) {
      showSaveChangesDialog = true;
    } else {
      model.UnlockDocument(document.id);
      stopEditProcedure(null);
      _router.navigate(RoutePaths.MENU.toUrl());
    }
  }

  void trashChangesAndStopEdit() async {
    stopEditProcedure(await model.getDocument(document.id));
  }

  //</editor-fold>

  //<editor-fold desc="Trackers">
  Object trackByIndex(int index, _) {
    return index;
  }

  Object trackBySnippetId(_, dynamic o) => o is Snippet ? o.snippetId : o;

  //</editor-fold>

  //<editor-fold desc="Overrides">
  @override
  void ngOnDestroy() {
    bodyStreamOnKeyDown.cancel();
    bodyStreamOnKeyUp.cancel();
  }

  @override
  void onActivate(_, RouterState current) async {
    final id = int.parse(current.parameters['id']);
    Future.wait([model.UpdateStructures(), model.SetLocalization()]).then((_) {
          () async {
        document = await model.getDocument(id);
      }();
    });
    HTML.document.execCommand("defaultParagraphSeparator", false, "p");
    bodyStreamOnKeyDown = body.onKeyDown.listen(handleKeyboardInputDown);
    bodyStreamOnKeyUp = body.onKeyUp.listen(handleKeyboardInputUp);
  }

  @override
  void ngAfterViewChecked() {
    if (document != null && document.setName) {
      document.setName = false;
      setTitle();
    }
  }

  //</editor-fold>

  //<editor-fold desc="Document Name Editing">
  void setTitle() {
    if (title != null && document != null) {
      title.innerText = document.name;
    }
  }

  void updateName(HTML.Event event) {
    document.name = (event.target as HTML.Element).innerText;
  }

  //</editor-fold>

  //<editor-fold desc="KeybindResolver">
  bool resolveKey(String code, bool ctrl, alt, shift, down) {
    Tuple2<KeyHandle, KeyHandle> tuple =
    _mapping[Tuple4<String, bool, bool, bool>(code, ctrl, alt, shift)];
    KeyHandle op = down ? tuple?.item1 : tuple?.item2;
    var ret = false;
    if (op != null) ret = op();
    return ret;
  }

  void handleKeyboardInputDown(HTML.KeyboardEvent event) {
    if (resolveKey(
        event.code, event.ctrlKey, event.altKey, event.shiftKey, true)) {
      event.preventDefault();
      event.stopPropagation();
    }
  }

  void handleKeyboardInputUp(HTML.KeyboardEvent event) {
    if (resolveKey(
        event.code, event.ctrlKey, event.altKey, event.shiftKey, false)) {
      event.preventDefault();
      event.stopPropagation();
    }
  }

  void setMapping() {
    //<editor-fold desc="Formatting">
    //<editor-fold desc="KeyQ Handling">
    _mapping[Tuple4<String, bool, bool, bool>("KeyQ", false, true, true)] =
        Tuple2<KeyHandle, KeyHandle>(() => allowEdit, () {
          HTML.Selection sel = HTML.window.getSelection();
          if (sel.anchorNode.parent == title) {
            return true;
          }
          if (allowEdit) HTML.document.execCommand("insertUnorderedList");
          return allowEdit;
        });
    //</editor-fold>
    //<editor-fold desc="KeyU Handling">
    _mapping[Tuple4<String, bool, bool, bool>("KeyU", true, false, false)] =
        Tuple2<KeyHandle, KeyHandle>(() => allowEdit, () {
          return allowEdit;
        });
    //</editor-fold>
    //<editor-fold desc="KeyI Handling">
    _mapping[Tuple4<String, bool, bool, bool>("KeyI", true, false, false)] =
        Tuple2<KeyHandle, KeyHandle>(() => allowEdit, () {
          HTML.Selection sel = HTML.window.getSelection();
          if (sel.anchorNode.parent == title) {
            return true;
          }
          if (allowEdit) HTML.document.execCommand("italic");
          return allowEdit;
        });
    //</editor-fold>
    //<editor-fold desc="KeyB Handling">
    _mapping[Tuple4<String, bool, bool, bool>("KeyB", true, false, false)] =
        Tuple2<KeyHandle, KeyHandle>(() => allowEdit, () {
          HTML.Selection sel = HTML.window.getSelection();
          if (sel.anchorNode.parent == title) {
            return true;
          }
          if (allowEdit) HTML.document.execCommand("bold");
          return allowEdit;
        });
    //</editor-fold>
    //<editor-fold desc="Backquote Handling">
    _mapping[
    Tuple4<String, bool, bool, bool>("Backquote", true, false, false)] =
        Tuple2<KeyHandle, KeyHandle>(() => allowEdit, () {
          HTML.Selection sel = HTML.window.getSelection();
          if (sel.anchorNode.parent == title) {
            return true;
          }
          if (allowEdit && !HTML.window
              .getSelection()
              .isCollapsed) {
            HTML.DocumentFragment docFragment =
            HTML.document.createDocumentFragment();
            HTML.Node newEle = HTML.document.createElement('code');
            //TODO:when this is fixed in library revert back to "HTML.window.getSelection().toString()"
            newEle.append(HTML.Text(
                context.callMethod('getSelection').callMethod('toString')));
            docFragment.append(newEle);
            HTML.Range range = sel.getRangeAt(0);
            range.deleteContents();
            range.insertNode(docFragment);
            for (int i = 0; i < newEle.parentNode.childNodes.length;) {
              if (newEle.parentNode.childNodes[i].text == "") {
                newEle.parentNode.childNodes[i].remove();
              } else {
                i++;
              }
            }
            int offset = newEle.text.length;
            while (newEle.nextNode != null &&
                newEle.nextNode.nodeName == "CODE") {
              newEle.text += newEle.nextNode.text;
              newEle.nextNode.remove();
            }
            while (newEle.previousNode != null &&
                newEle.previousNode.nodeName == "CODE") {
              offset += newEle.previousNode.text.length;
              newEle.text = newEle.previousNode.text + newEle.text;
              newEle.previousNode.remove();
            }
            range = HTML.document.createRange();
            range.setStart(newEle.firstChild, offset);
            range.collapse(true);
            sel.removeAllRanges();
            sel.addRange(range);
            return true;
          }
          return allowEdit;
        });
    //</editor-fold>
    //</editor-fold>
    //<editor-fold desc="Heading Handling">
    //<editor-fold desc="Digit1 Handling">
    _mapping[Tuple4<String, bool, bool, bool>("Digit1", false, true, true)] =
        Tuple2<KeyHandle, KeyHandle>(() => allowEdit, () {
          if (!allowEdit) return true;
          HTML.Selection sel = HTML.window.getSelection();
          if (sel.anchorNode.parent == title) {
            return true;
          }
          HTML.Node blockNode = sel
              .getRangeAt(0)
              .startContainer
              .parentNode;
          while (!isBlockTag(blockNode.nodeName)) {
            blockNode = blockNode.parentNode;
          }
          if (blockNode.nodeName == "LI") return true;
          if (blockNode.nodeName == "H2") {
            HTML.document.execCommand("formatBlock", false, "<p>");
          } else {
            HTML.document.execCommand("formatBlock", false, "<h2>");
          }
          return allowEdit;
        });
    //</editor-fold>
    //<editor-fold desc="Digit2 Handling">
    _mapping[Tuple4<String, bool, bool, bool>("Digit2", false, true, true)] =
        Tuple2<KeyHandle, KeyHandle>(() => allowEdit, () {
          if (!allowEdit) return true;
          HTML.Selection sel = HTML.window.getSelection();
          if (sel.anchorNode.parent == title) {
            return true;
          }
          HTML.Node blockNode = sel
              .getRangeAt(0)
              .startContainer
              .parentNode;
          while (!isBlockTag(blockNode.nodeName)) {
            blockNode = blockNode.parentNode;
          }
          if (blockNode.nodeName == "LI") return true;
          if (blockNode.nodeName == "H3") {
            HTML.document.execCommand("formatBlock", false, "<p>");
          } else {
            HTML.document.execCommand("formatBlock", false, "<h3>");
          }
          return allowEdit;
        });
    //</editor-fold>
    //<editor-fold desc="Digit3 Handling">
    _mapping[Tuple4<String, bool, bool, bool>("Digit3", false, true, true)] =
        Tuple2<KeyHandle, KeyHandle>(() => allowEdit, () {
          if (!allowEdit) return true;
          HTML.Selection sel = HTML.window.getSelection();
          if (sel.anchorNode.parent == title) {
            return true;
          }
          HTML.Node blockNode = sel
              .getRangeAt(0)
              .startContainer
              .parentNode;
          while (!isBlockTag(blockNode.nodeName)) {
            blockNode = blockNode.parentNode;
          }
          if (blockNode.nodeName == "LI") return true;
          if (blockNode.nodeName == "H4") {
            HTML.document.execCommand("formatBlock", false, "<p>");
          } else {
            HTML.document.execCommand("formatBlock", false, "<h4>");
          }
          return allowEdit;
        });
    //</editor-fold>
    //<editor-fold desc="Digit4 Handling">
    _mapping[Tuple4<String, bool, bool, bool>("Digit4", false, true, true)] =
        Tuple2<KeyHandle, KeyHandle>(() => allowEdit, () {
          if (!allowEdit) return true;
          HTML.Selection sel = HTML.window.getSelection();
          if (sel.anchorNode.parent == title) {
            return true;
          }
          HTML.Node blockNode = sel
              .getRangeAt(0)
              .startContainer
              .parentNode;
          while (!isBlockTag(blockNode.nodeName)) {
            blockNode = blockNode.parentNode;
          }
          if (blockNode.nodeName == "LI") return true;
          if (blockNode.nodeName == "H5") {
            HTML.document.execCommand("formatBlock", false, "<p>");
          } else {
            HTML.document.execCommand("formatBlock", false, "<h5>");
          }
          return allowEdit;
        });
    //</editor-fold>
    //<editor-fold desc="Digit5 Handling">
    _mapping[Tuple4<String, bool, bool, bool>("Digit5", false, true, true)] =
        Tuple2<KeyHandle, KeyHandle>(() => allowEdit, () {
          if (!allowEdit) return true;
          HTML.Selection sel = HTML.window.getSelection();
          if (sel.anchorNode.parent == title) {
            return true;
          }
          HTML.Node blockNode = sel
              .getRangeAt(0)
              .startContainer
              .parentNode;
          while (!isBlockTag(blockNode.nodeName)) {
            blockNode = blockNode.parentNode;
          }
          if (blockNode.nodeName == "LI") return true;
          if (blockNode.nodeName == "H6") {
            HTML.document.execCommand("formatBlock", false, "<p>");
          } else {
            HTML.document.execCommand("formatBlock", false, "<h6>");
          }
          return allowEdit;
        });
    //</editor-fold>
    //</editor-fold>
    //<editor-fold desc="Enter Handling">
    _mapping[Tuple4<String, bool, bool, bool>("Enter", false, false, false)] =
        Tuple2<KeyHandle, KeyHandle>(() {
          if (!allowEdit) return false;
          HTML.Selection sel = HTML.window.getSelection();
          if (sel.anchorNode.parent == title) {
            return true;
          }
          return false;
        }, () {
          if (!allowEdit) return false;
          HTML.Selection sel = HTML.window.getSelection();
          if (sel.anchorNode.parent == title) {
            return true;
          }
          return false;
        });
    _mapping[Tuple4<String, bool, bool, bool>("Enter", false, false, true)] =
        Tuple2<KeyHandle, KeyHandle>(() => allowEdit, () {
          if (!allowEdit) return false;
          HTML.Selection sel = HTML.window.getSelection();
          if (sel.anchorNode.parent == title) {
            return true;
          }
          HTML.DocumentFragment docFragment =
          HTML.document.createDocumentFragment();
          HTML.Node newEle = HTML.document.createElement('br');
          docFragment.append(newEle);
          HTML.Range range = sel.getRangeAt(0);
          range.deleteContents();
          range.insertNode(docFragment);
          HTML.Node next = newEle;
          while (true) {
            if (next.nextNode == null) break;
            if (next.nextNode.text != "") {
              next = next.nextNode;
            } else {
              next.nextNode.remove();
            }
          }
          if (newEle.nextNode == null &&
              (newEle.previousNode == null ||
              newEle.previousNode.nodeName != "BR")) {
            HTML.Node parent = newEle.parentNode;
            newEle = HTML.document.createElement('br');
            parent.append(newEle);
          }
          range = HTML.document.createRange();
          range.setStartAfter(newEle);
          range.collapse(true);
          sel.removeAllRanges();
          sel.addRange(range);
          return true;
        });
    //</editor-fold>
    //<editor-fold desc="Escape Handling">
    _mapping[Tuple4<String, bool, bool, bool>("Escape", false, false, false)] =
        Tuple2<KeyHandle, KeyHandle>(() => moveMode || dialogOpen(), () {
          bool ret = moveMode || dialogOpen();
          if (moveMode) {
            startMoveMode(null);
          }
          if (dialogOpen()) dialogClose();
          return ret;
        });
    //</editor-fold>
    //<editor-fold desc="Space Handling">
    _mapping[Tuple4<String, bool, bool, bool>("Space", true, false, false)] =
        Tuple2<KeyHandle, KeyHandle>(() => allowEdit, () {
          HTML.Selection sel = HTML.window.getSelection();
          if (sel.anchorNode.parent == title) {
            return true;
          }
          if (allowEdit) HTML.document.execCommand("removeFormat");
          return allowEdit;
        });
    //</editor-fold>
  }

//</editor-fold>
  bool dialogOpen() {
    return showSaveChangesDialog ||
        showRemoveSnippetDialog ||
        showCannotEditDialog1 ||
        showCannotEditDialog2 ||
        showDeleteDocumentDialog ||
        showDeleteDocumentErrorDialog ||
        showRenameUnsuccessfulDialog ||
        showEditRulesetDialog ||
        showConfigureLanguageDialog;
  }

  void dialogClose() {
    showSaveChangesDialog = showRemoveSnippetDialog = showCannotEditDialog1 =
        showCannotEditDialog2 = showDeleteDocumentDialog =
        showDeleteDocumentErrorDialog = showRenameUnsuccessfulDialog =
        showEditRulesetDialog = showConfigureLanguageDialog = false;
  }
}

typedef KeyHandle();

bool isBlockTag(String tag) {
  //todo: make this RegEx
  switch (tag.toLowerCase()) {
    case "address":
    case "article":
    case "aside":
    case "blockquote":
    case "details":
    case "dialog":
    case "dd":
    case "div":
    case "dl":
    case "dt":
    case "fieldset":
    case "figcaption":
    case "figure":
    case "footer":
    case "form":
    case "h1":
    case "h2":
    case "h3":
    case "h4":
    case "h5":
    case "h6":
    case "header":
    case "hgroup":
    case "hr":
    case "li":
    case "main":
    case "nav":
    case "ol":
    case "p":
    case "pre":
    case "section":
    case "table":
    case "ul":
      return true;
      break;
    default:
      return false;
  }
}
