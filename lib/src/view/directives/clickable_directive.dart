import 'dart:async';
import 'dart:html';

import 'package:angular/angular.dart';

@Directive(
  selector: '[clickable]',
)
class ClickableDirective {
  bool _clickable;

  final _trigger = StreamController<UIEvent>.broadcast(sync: true);

  @Input('clickableClass')
  String className;

  final Element _el;

  ClickableDirective(this._el);

  @Input()
  set clickable(bool val) {
    _clickable = val;
    _restyle();
  }

  @Output()
  Stream<UIEvent> get trigger => _trigger.stream;

  @HostListener('click')
  void handleClick(MouseEvent mouseEvent) {
    if (!_clickable) return;
    _trigger.add(mouseEvent);
  }

//  This part is mostly copied from ButtonDirective of Angular dart component
  void _restyle() {
    _el.style.cursor = _clickable ? "pointer" : "auto";
    if (className != null && className != "") {
      if (_clickable) {
        _el.classes.add(className);
      } else {
        _el.classes.remove(className);
      }
    }
  }
}
