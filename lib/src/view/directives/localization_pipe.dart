import 'dart:core';

import 'package:angular/angular.dart';
import 'package:Internal_lore/src/model/model.dart';

@Pipe('local')
class LocalizationPipe extends PipeTransform {
  String transform(Language language, String identifier) {
    if (language == null) return identifier;
    return language.Render(identifier);
  }
}
