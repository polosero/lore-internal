import 'dart:core';

import 'package:Internal_lore/src/model/model.dart';
import 'package:Internal_lore/src/view/directives/localization_pipe.dart';
import 'package:angular/angular.dart';
import 'package:angular_components/laminate/popup/module.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_icon/material_icon.dart';
import 'package:angular_components/material_select/material_dropdown_select.dart';
import 'package:angular_components/material_select/material_select_searchbox.dart';

@Component(
    selector: 'select-rule-dropdown',
    templateUrl: 'select_rule_dropdown_component.html',
    directives: [
      coreDirectives,
      MaterialIconComponent,
      MaterialButtonComponent,
      MaterialDropdownSelectComponent,
      MaterialSelectSearchboxComponent,
    ],
    styleUrls: [
      "select_rule_dropdown_component.css"
    ],
    providers: [
      popupBindings
    ],
    exports: [
      Model
    ],
    pipes: [
      LocalizationPipe
    ])
class SelectRuleDropdown {
  @Input()
  Rule<String> rule;

  SelectRuleDropdown();

  String get selectLabel =>
      rule.value != null
          ? Renderer(rule.value)
          : Model.localization.currentLanguage.Render(
          "select_placeholder_value");

  String Renderer(dynamic s) => rule.rType.values[s].label;
}
