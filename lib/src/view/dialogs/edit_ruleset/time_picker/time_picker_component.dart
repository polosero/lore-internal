import 'dart:core';

import 'package:Internal_lore/src/model/model.dart';
import 'package:angular/angular.dart';
import 'package:angular_components/material_datepicker/material_date_time_picker.dart';
import 'package:angular_components/material_datepicker/module.dart';
import 'package:angular_components/utils/browser/window/module.dart';

@Component(
  selector: 'time-picker',
  templateUrl: 'time_picker_component.html',
  directives: [MaterialDateTimePickerComponent],
  styleUrls: ["time_picker_component.css"],
  providers: [windowBindings, datepickerBindings],
)
class TimePickerComponent {
  @Input('time')
  DateTimeRule rule;

  TimePickerComponent();
}
