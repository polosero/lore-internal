import 'dart:async';
import 'dart:core';

import 'package:Internal_lore/src/view/directives/localization_pipe.dart';
import 'package:Internal_lore/src/model/model.dart';
import 'package:Internal_lore/src/view/dialogs/edit_ruleset/select_rule_dropdown/select_rule_dropdown_component.dart';
import 'package:Internal_lore/src/view/dialogs/edit_ruleset/time_picker/time_picker_component.dart';
import 'package:angular/angular.dart';
import 'package:angular_components/laminate/components/modal/modal.dart';
import 'package:angular_components/laminate/overlay/module.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_dialog/material_dialog.dart';
import 'package:angular_components/material_icon/material_icon.dart';
import 'package:angular_components/material_input/material_input.dart';
import 'package:angular_components/material_input/material_number_accessor.dart';
import 'package:angular_components/material_select/material_dropdown_select.dart';
import 'package:angular_components/material_select/material_select_searchbox.dart';
import 'package:angular_components/model/selection/string_selection_options.dart';

@Component(
  selector: 'edit-ruleset-dialog',
  templateUrl: 'edit_ruleset_component.html',
  directives: [
    SelectRuleDropdown,
    TimePickerComponent,
    MaterialButtonComponent,
    MaterialDialogComponent,
    ModalComponent,
    coreDirectives,
    MaterialIconComponent,
    MaterialDropdownSelectComponent,
    MaterialSelectSearchboxComponent,
    materialInputDirectives,
    materialNumberInputDirectives,
  ],
  styleUrls: ["edit_ruleset_component.css"],
  providers: [overlayBindings],
  exports: [InputType, RuleType, Model],
  pipes: [LocalizationPipe],
)
class EditRulesetDialog implements OnInit, OnDestroy {
  //<editor-fold desc="Inputs">
  @Input()
  Ruleset ruleset;

  //</editor-fold>

  //<editor-fold desc="Properties">
  RuleType selectedRule = null;
  int numberOfNotUsedRules;
  StringSelectionOptions<RuleType> NotUsedRules;
  bool dialog = true;

  //</editor-fold>

  //<editor-fold desc="Outputs">
  final _close = StreamController<bool>();

  @Output()
  Stream<bool> get closeStream => _close.stream;

  //</editor-fold>


  String get selectRuleLabel =>
      selectedRule != null
          ? RuleType.Renderer(selectedRule)
          : Model.localization.currentLanguage.Render(
          "select_placeholder_rule");

  void AddRule() {
    if (selectedRule == null) return;
    Rule<dynamic> rule = Ruleset.toRule(selectedRule, null);
    ruleset.rules.add(rule);
    selectedRule = null;
    updateNotUsedRules();
  }



  void stopDialog() {
    dialog = false;
    ruleset.updateParsed();
    _close.add(false);
  }

  Object trackByIndex(int index, _) {
    return index;
  }

  void updateNotUsedRules() {
    List<RuleType> NURes = List.from(ruleset.typeList);
    ruleset.rules.forEach((rule) {
      NURes.remove(rule.rType);
    });
    numberOfNotUsedRules = NURes.length;
    NotUsedRules = StringSelectionOptions<RuleType>(NURes,
        toFilterableString: RuleType.Renderer);
    if (NotUsedRules.optionsList.isNotEmpty) {
      selectedRule = NotUsedRules.optionsList.first;
    }
  }

  //<editor-fold desc="Overrides">
  @override
  void ngOnInit() {
    updateNotUsedRules();
  }

  @override
  void ngOnDestroy() {
    _close.close();
  }
//</editor-fold>
}
