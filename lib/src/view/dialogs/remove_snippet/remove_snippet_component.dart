import 'dart:async';
import 'dart:core';

import 'package:Internal_lore/src/view/directives/localization_pipe.dart';
import 'package:Internal_lore/src/model/model.dart';
import 'package:angular/angular.dart';
import 'package:angular_components/auto_dismiss/auto_dismiss.dart';
import 'package:angular_components/laminate/components/modal/modal.dart';
import 'package:angular_components/laminate/overlay/module.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_dialog/material_dialog.dart';
import 'package:angular_components/material_icon/material_icon.dart';

@Component(
  selector: 'remove-snippet-dialog',
  templateUrl: 'remove_snippet_component.html',
  directives: [
    MaterialButtonComponent,
    MaterialDialogComponent,
    ModalComponent,
    coreDirectives,
    MaterialIconComponent,
    AutoDismissDirective
  ],
  styleUrls: ["remove_snippet_component.css"],
  providers: [overlayBindings],
  pipes: [LocalizationPipe],
  exports: [Model],
)
class RemoveSnippetDialog implements OnDestroy {
  final _close = StreamController<bool>();
  final _trigger = StreamController<bool>();
  bool dialog = true;
  @Output()
  Stream<bool> get closeStream => _close.stream;

  @Output()
  Stream<bool> get trigger => _trigger.stream;

  @override
  void ngOnDestroy() {
    _close.close();
    _trigger.close();
  }

  void stopDialog() {
    dialog = false;
    _close.add(false);
  }

  void sendTrigger() {
    _trigger.add(true);
    stopDialog();
  }
}
