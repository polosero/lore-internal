import 'dart:async';
import 'dart:core';

import 'package:Internal_lore/src/view/directives/localization_pipe.dart';
import 'package:Internal_lore/src/model/model.dart';
import 'package:angular/angular.dart';
import 'package:angular_components/auto_dismiss/auto_dismiss.dart';
import 'package:angular_components/laminate/components/modal/modal.dart';
import 'package:angular_components/laminate/overlay/module.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_dialog/material_dialog.dart';
import 'package:angular_components/material_icon/material_icon.dart';
import 'package:angular_forms/angular_forms.dart';

@Component(
  selector: 'new-document-dialog',
  templateUrl: 'new_document_component.html',
  directives: [
    MaterialButtonComponent,
    MaterialDialogComponent,
    ModalComponent,
    coreDirectives,
    MaterialIconComponent,
    AutoDismissDirective,
    formDirectives,
  ],
  styleUrls: ["new_document_component.css"],
  providers: [overlayBindings],
  pipes: [LocalizationPipe],
  exports: [Model],
)
class NewDocumentDialog implements OnDestroy {
  bool showError = false;
  Model model;
  final _close = StreamController<bool>();
  bool dialog = true;
  NewDocumentDialog(this.model);

  @Output()
  Stream<bool> get closeStream => _close.stream;

  @override
  void ngOnDestroy() {
    _close.close();
  }

  void stopDialog() {
    dialog = false;
    _close.add(false);
  }

  void onSubmit(String data) async {
    if (await model.NewDocument(data)) {
      model.UpdateStructures();
      stopDialog();
    } else {
      showError = true;
    }
  }
}
