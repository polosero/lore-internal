import 'dart:async';
import 'dart:core';

import 'package:Internal_lore/src/view/directives/localization_pipe.dart';
import 'package:Internal_lore/src/model/model.dart';
import 'package:angular/angular.dart';
import 'package:angular_components/auto_dismiss/auto_dismiss.dart';
import 'package:angular_components/laminate/components/modal/modal.dart';
import 'package:angular_components/laminate/overlay/module.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_dialog/material_dialog.dart';
import 'package:angular_components/material_icon/material_icon.dart';

@Component(
  selector: 'cannot-edit-1-dialog',
  templateUrl: 'cannot_edit_1_component.html',
  directives: [
    MaterialButtonComponent,
    MaterialDialogComponent,
    ModalComponent,
    coreDirectives,
    MaterialIconComponent,
    AutoDismissDirective
  ],
  styleUrls: ["cannot_edit_1_component.css"],
  providers: [overlayBindings],
  pipes: [LocalizationPipe],
  exports: [Model],
)
class CannotEdit1Dialog implements OnDestroy {
  bool dialog = true;
  final _close = StreamController<bool>();

  @Output()
  Stream<bool> get closeStream => _close.stream;

  @override
  void ngOnDestroy() {
    _close.close();
  }

  void stopDialog() {
    dialog = false;
    _close.add(false);
  }
}
