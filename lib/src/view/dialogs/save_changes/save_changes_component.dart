import 'dart:async';
import 'dart:core';

import 'package:Internal_lore/src/view/directives/localization_pipe.dart';
import 'package:Internal_lore/src/model/model.dart';
import 'package:angular/angular.dart';
import 'package:angular_components/auto_dismiss/auto_dismiss.dart';
import 'package:angular_components/laminate/components/modal/modal.dart';
import 'package:angular_components/laminate/overlay/module.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_dialog/material_dialog.dart';
import 'package:angular_components/material_icon/material_icon.dart';

@Component(
  selector: 'save-changes-dialog',
  templateUrl: 'save_changes_component.html',
  directives: [
    MaterialButtonComponent,
    MaterialDialogComponent,
    ModalComponent,
    coreDirectives,
    MaterialIconComponent,
    AutoDismissDirective
  ],
  styleUrls: ["save_changes_component.css"],
  providers: [overlayBindings],
  pipes: [LocalizationPipe],
  exports: [Model],
)
class SaveChangesDialog implements OnDestroy {
  final _close = StreamController<bool>();
  final _trigger1 = StreamController<bool>();
  final _trigger2 = StreamController<bool>();
  bool dialog = true;
  @Input()
  Duration lockDuration;

  @Output()
  Stream<bool> get closeStream => _close.stream;

  @Output()
  Stream<bool> get trigger1 => _trigger1.stream;

  @Output()
  Stream<bool> get trigger2 => _trigger2.stream;

  @override
  void ngOnDestroy() {
    _close.close();
    _trigger1.close();
    _trigger2.close();
  }

  void sendTrigger1() {
    _trigger1.add(true);
    stopDialog();
  }

  void sendTrigger2() {
    _trigger2.add(true);
    stopDialog();
  }

  void stopDialog() {
    dialog = false;
    _close.add(false);
  }
}
