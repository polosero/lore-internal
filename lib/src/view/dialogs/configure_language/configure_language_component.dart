import 'dart:async';
import 'dart:core';

import 'package:Internal_lore/src/view/directives/localization_pipe.dart';
import 'package:Internal_lore/src/model/model.dart';
import 'package:angular/angular.dart';
import 'package:angular_components/laminate/components/modal/modal.dart';
import 'package:angular_components/laminate/overlay/module.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_dialog/material_dialog.dart';
import 'package:angular_components/material_icon/material_icon.dart';
import 'package:angular_components/material_select/material_dropdown_select.dart';
import 'package:angular_components/material_select/material_select_searchbox.dart';

@Component(
  selector: 'configure-language-dialog',
  templateUrl: 'configure_language_component.html',
  directives: [
    MaterialButtonComponent,
    MaterialDialogComponent,
    ModalComponent,
    coreDirectives,
    MaterialIconComponent,
    MaterialDropdownSelectComponent,
    MaterialSelectSearchboxComponent,
  ],
  styleUrls: ["configure_language_component.css"],
  providers: [overlayBindings],
  exports: [Model],
  pipes: [LocalizationPipe],
)
class ConfigureLanguageDialog implements OnDestroy, OnInit {
  final _close = StreamController<bool>();
  bool dialog = true;

  String selectedLanguage;
  Model model;

  @Output()
  Stream<bool> get closeStream => _close.stream;

  String get selectLanguageLabel => selectedLanguage != null
      ? Model.localization.OptionRenderer(selectedLanguage)
      : Model.localization.currentLanguage
          .Render('select_placeholder_language');

  @override
  void ngOnDestroy() {
    _close.close();
  }

  void stopDialog() {
    model.SetLocalization(id: selectedLanguage);
    dialog = false;
    _close.add(false);
  }

  Object trackByIndex(int index, _) {
    return index;
  }

  @override
  void ngOnInit() {
    selectedLanguage = Model.localization.currentLanguage.id;
  }

  ConfigureLanguageDialog(this.model);
}
