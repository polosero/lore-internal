import 'package:Internal_lore/src/model/model.dart';
import 'package:Internal_lore/src/network/gateway_service.dart';
import 'package:Internal_lore/src/network/network_service.dart';
import 'package:Internal_lore/src/routes.dart';
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';

@Component(selector: 'app',
    templateUrl: "app_component.html",
    providers: [
      ClassProvider(Model),
      ClassProvider(NetworkService),
      ClassProvider(Gateway)
    ],
    directives: [
      routerDirectives
    ],
    exports: [
      RoutePaths,
      Routes
    ],
    styleUrls: [
      "app_component.css"
    ])
class AppComponent {
  AppComponent();
}
